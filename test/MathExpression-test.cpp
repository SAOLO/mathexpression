#include "catch.hpp"
#include "MathExpression.h"

/*A test designed for the class MathExpression which should hold the functions of a simple calculator*/

TEST_CASE("Testing math expressions for correct input") {
    SECTION("It should not  accept letters") {

        MathExpression me("Hello World");
        REQUIRE_FALSE(me.isValid());
        SECTION("It should return an error message") {
            REQUIRE(me.errorMessage() == "Error");
            SECTION("To be sure we check the other functions aren't reacheable") {
                REQUIRE_THROWS(me.calculate());
                REQUIRE_THROWS(me.infixNotation());
                REQUIRE_THROWS(me.postfixNotation());
            }
        }
    }

    SECTION("It should not accept an operator with only one operand") {
        MathExpression me("35+");
        REQUIRE_FALSE(me.isValid());
        SECTION("It should return an error message") {
            REQUIRE(me.errorMessage() == "Error");
            SECTION("To be sure we check the other functions aren't reacheable") {
                REQUIRE_THROWS(me.calculate());
                REQUIRE_THROWS(me.infixNotation());
                REQUIRE_THROWS(me.postfixNotation());
            }
        }
    }

    SECTION("It can not calculate compound expressions without an arithmetic operator as link") {
        MathExpression me("(2*4)(3/1)");
        REQUIRE_FALSE(me.isValid());
        SECTION("It should return an error message") {
            REQUIRE(me.errorMessage() == "Error");
            SECTION("To be sure we check the other functions aren't reacheable") {
                REQUIRE_THROWS(me.calculate());
                REQUIRE_THROWS(me.infixNotation());
                REQUIRE_THROWS(me.postfixNotation());
            }
        }
    }

    SECTION("An opening bracket must have a closing bracket") {
        MathExpression me("(5-3");
        REQUIRE_FALSE(me.isValid());
        SECTION("It should return an error message") {
            REQUIRE(me.errorMessage() == "Error");
            SECTION("To be sure we check the other functions aren't reacheable") {
                REQUIRE_THROWS(me.calculate());
                REQUIRE_THROWS(me.infixNotation());
                REQUIRE_THROWS(me.postfixNotation());
            }
        }
    }


    SECTION("Writing a set of numbers is not accepted") {
        MathExpression me("10 8 6");
        REQUIRE_FALSE(me.isValid());
        SECTION("It should return an error message") {
            REQUIRE(me.errorMessage() == "Error");
            SECTION("To be sure we check the other functions aren't reacheable") {
                REQUIRE_THROWS(me.calculate());
                REQUIRE_THROWS(me.infixNotation());
                REQUIRE_THROWS(me.postfixNotation());
            }
        }
    }

    SECTION("Test so it does not accept multiple operators in a row") {
        MathExpression me("9++7");
        REQUIRE_FALSE(me.isValid());
        SECTION("It should return an error message") {
            REQUIRE(me.errorMessage() == "Error");
            SECTION("To be sure we check the other functions aren't reacheable") {
                REQUIRE_THROWS(me.calculate());
                REQUIRE_THROWS(me.infixNotation());
                REQUIRE_THROWS(me.postfixNotation());
            }
        }
    }


    SECTION("Working with brackets and th multiply operator") {
        MathExpression me("(32+8)-(2*2)");
        SECTION("Then isValid will be true") {
            REQUIRE(me.isValid());
            SECTION("infixNotation should be the same as constructor input") {
                REQUIRE(me.infixNotation() == "(32+8)-(2*2)");
                SECTION("postfixNotation should be correct") {
                    REQUIRE(me.postfixNotation() == "32 8 + 2 2 * -");
                    SECTION("And of course the mathematical expression should have te correcct value")
                    REQUIRE(me.calculate() ==  36);
                }
            }
        }
    }


    //Start testing if it work if input is valid

    SECTION("Still working with brackets and the multiply operator") {
        MathExpression me("3*(11+1)-4");
        SECTION("Then isValid will be true") {
            REQUIRE(me.isValid());
            SECTION("infixNotation should be the same as constructor input") {
                REQUIRE(me.infixNotation() == "3*(11+1)-4");
                SECTION("postfixNotation should be correct") {
                    REQUIRE(me.postfixNotation() == "3 11 1 + * 4 -");
                    SECTION("And of course the mathematical expression should have te correcct value")
                        REQUIRE(me.calculate() ==  32);
                }
            }
        }
    }


    SECTION("Solitary numbers in brackets, one operator. Also check if floats work") {
        MathExpression me("(5)/(8)");
        SECTION("Then isValid will be true") {
            REQUIRE(me.isValid());
            SECTION("infixNotation should be the same as constructor input") {
                REQUIRE(me.infixNotation() == "(5)/(8)");
                SECTION("postfixNotation should be correct") {
                    REQUIRE(me.postfixNotation() == "5 8 /");
                    SECTION("And of course the mathematical expression should have te correcct value")
                        REQUIRE(me.calculate() ==  Approx(0.625).epsilon(0.01));
                }
            }
        }
    }

    SECTION("Solitary number") {
        MathExpression me("2");
        SECTION("isValid will be true") {
            REQUIRE(me.isValid());
            SECTION("infixNotation should be the same as constructor input") {
                REQUIRE(me.infixNotation() == "2");
                SECTION("Check that postfixNotation is correct") {
                    REQUIRE(me.postfixNotation() == "2");
                    SECTION("Check if value of expression is correct") {
                        REQUIRE(me.calculate() == 2);
                    }
                }
            }
        }
    }


    SECTION("Solitary number in brackets") {
        MathExpression me("(9)");
        SECTION("isValid will be true") {
            REQUIRE(me.isValid());
            SECTION("infixNotation should be the same as constructor input") {
                REQUIRE(me.infixNotation() == "(9)");
                SECTION("Check that postfixNotation is correct") {
                    REQUIRE(me.postfixNotation() == "9");
                    SECTION("Check if value of expression is correct") {
                        REQUIRE(me.calculate() == 9);
                    }
                }
            }
        }
    }


    SECTION("Test assignment operator") {
        MathExpression me("(9)");
        me = "8+9";
        SECTION("isValid will be true") {
            REQUIRE(me.isValid());
            SECTION("infixNotation should the assignment operator value") {
                REQUIRE(me.infixNotation() == "8+9");
                SECTION("Check that postfixNotation is correct") {
                    REQUIRE(me.postfixNotation() == "8 9 +");
                    SECTION("Check if value of expression is correct") {
                        REQUIRE(me.calculate() == 17);
                    }
                }
            }
        }
    }
}
